<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%.o ^6Fc2l,IA}8H543Dw_U@2hZ(EGiyO@HO7:Ac^+4GCHsF_#H&NBlOpuIu4`+.');
define('SECURE_AUTH_KEY',  '|$x=wx:W*G-,O`$Ds0.j2NSY*=p:Jxy;c?s[+Dd05G)hkV.?hb=a@L<?.h J}G)l');
define('LOGGED_IN_KEY',    '-.Cg/kTVO5b55,h[]]`y(&I-+nsA&>VgbDbG}(uP:Iy86R`S7BEnzyBvG#k`Riy1');
define('NONCE_KEY',        'q1Vt4{J+!,QoE}pH&0`89>5UQ#*@2#7,-v_>5zRk&e+iD:4RD%S|=SwD;!HVM~]1');
define('AUTH_SALT',        '`@ADHQ)CB{o0#Iy{`+RU`,-wNb+#0P!m|Cr17{$L/+B;1cuuaxZRpOxbo<05V$NO');
define('SECURE_AUTH_SALT', '@-wQm/nCOg-(W6>/xSK|CduSXfpwTC~g4^r;%~3I::iRxm|$3MZfJHCND Do(Ym4');
define('LOGGED_IN_SALT',   'B#/]m{,+sbUo*m37e~o-iyV*UeIQ#NCaH;6-^@vB=z4(4L=_9oOH$i/ }i Npv?d');
define('NONCE_SALT',       'H%m^L76)~=-kiFlgEC57P;r-uA&aac5C#+$VsB!~y{Q$8RkR?%zG-7mbYBak$f:/');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_yummy';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', false);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');