LICENSE
=======

- This theme, like WordPress, is licensed under the GPL.

- REM unit polyfill -
	REM-unit-polyfill by @chuckcarpenter - https://github.com/chuckcarpenter/REM-unit-polyfill - @REM-unit-polyfill
	License - http://chuckcarpenter.mit-license.org/ ( MIT License )

- Flexslider
	Flexslider by @woothemes - https://github.com/woothemes/FlexSlider - @FlexSlider
	License - https://github.com/woothemes/FlexSlider/blob/master/LICENSE.md ( GPLv2 )

- jQuery.mmenu
	jQuery.mmenu by @BeSite - https://github.com/BeSite/jQuery.mmenu - @jQuery.mmenu
	License - http://en.wikipedia.org/wiki/MIT_License ( Dual licensed under the MIT license )

- Modernizr
	Modernizr 2.8.3 by @Modernizr - https://github.com/Modernizr/Modernizr - @Modernizr
	License - http://opensource.org/licenses/MIT ( MIT License )

- FontAwesome
	Font Awesome 4.3.0 by @davegandy - http://fontawesome.io - @fontawesome
	License - http://fontawesome.io/license ( Font: SIL OFL 1.1, CSS: MIT License )

-Animate
	Animate by @daneden - https://github.com/daneden/animate.css - @animate.css
	License - http://opensource.org/licenses/MIT ( MIT License )

-Hover
	Hover by @Ian Lunn - https://github.com/IanLunn/Hover/ - @hover.css
	License - http://opensource.org/licenses/MIT ( MIT License )

-Bootstrap
	_s by @Automattic - https://github.com/automattic/_s - @_s
	License - https://github.com/Automattic/_s/commit/60d249b7c31733181e2d213784110f221a26ecc0 ( GPLv2 License )


IMAGE SIZE
==========

-97*64pixels


IMAGE LINK
==========

-slide1.jpg
	@Link - https://pixabay.com/en/book-read-relax-lilac-bank-old-759873/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide2.jpg
	@Link - https://pixabay.com/en/happy-couple-young-people-family-692725/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide3.jpg
	@Link - https://pixabay.com/en/blonde-girl-young-woman-person-768681/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide4.jpg
	@Link - https://pixabay.com/en/girls-friends-friendship-young-685778/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide5.jpg
	@Link - https://pixabay.com/en/bride-bouquet-wedding-white-woman-845727/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide6.jpg
	@Link - https://pixabay.com/en/meal-food-decorative-restaurant-731422/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide7.jpg
	@Link - https://pixabay.com/en/fashion-girl-woman-female-model-821506/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

-slide8.jpg
	@Link - https://pixabay.com/en/pier-wooden-planks-shore-water-336599/ - @pixabay
	License - https://creativecommons.org/publicdomain/zero/1.0/deed.en ( CC0 1.0 Universal (CC0 1.0) )

DEMO URL
========

	@Link - http://www.supernovathemes.com/powen/


THEME DOCUMENTATION
===================

	@Link - http://supernovathemes.com/powen-theme/
