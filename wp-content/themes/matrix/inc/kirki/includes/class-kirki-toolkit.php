<?php
/**
 * The main Kirki object
 *
 * @package     Kirki
 * @category    Core
 * @author      Aristeides Stathopoulos
 * @copyright   Copyright (c) 2015, Aristeides Stathopoulos
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

// Early exit if the class already exists
if (class_exists('Kirki_Toolkit')) {
    return;
}

final class Kirki_Toolkit
{

    /** @var Kirki The only instance of this class */
    public static $instance = null;

    public static $version = '1.0.2';

    public $font_registry = null;
    public $scripts = null;
    public $api = null;
    public $styles = array();

    /**
     * Access the single instance of this class
     * @return Kirki
     */
    public static function get_instance()
    {
        if (null == self::$instance) {
            self::$instance = new Kirki_Toolkit();
        }
        return self::$instance;
    }

    /**
     * Shortcut method to get the translation strings
     */
    public static function i18n()
    {

        $i18n = array(
            'background-color' => __('Background Color', 'matrix'),
            'background-image' => __('Background Image', 'matrix'),
            'no-repeat' => __('No Repeat', 'matrix'),
            'repeat-all' => __('Repeat All', 'matrix'),
            'repeat-x' => __('Repeat Horizontally', 'matrix'),
            'repeat-y' => __('Repeat Vertically', 'matrix'),
            'inherit' => __('Inherit', 'matrix'),
            'background-repeat' => __('Background Repeat', 'matrix'),
            'cover' => __('Cover', 'matrix'),
            'contain' => __('Contain', 'matrix'),
            'background-size' => __('Background Size', 'matrix'),
            'fixed' => __('Fixed', 'matrix'),
            'scroll' => __('Scroll', 'matrix'),
            'background-attachment' => __('Background Attachment', 'matrix'),
            'left-top' => __('Left Top', 'matrix'),
            'left-center' => __('Left Center', 'matrix'),
            'left-bottom' => __('Left Bottom', 'matrix'),
            'right-top' => __('Right Top', 'matrix'),
            'right-center' => __('Right Center', 'matrix'),
            'right-bottom' => __('Right Bottom', 'matrix'),
            'center-top' => __('Center Top', 'matrix'),
            'center-center' => __('Center Center', 'matrix'),
            'center-bottom' => __('Center Bottom', 'matrix'),
            'background-position' => __('Background Position', 'matrix'),
            'background-opacity' => __('Background Opacity', 'matrix'),
            'on' => __('ON', 'matrix'),
            'off' => __('OFF', 'matrix'),
            'all' => __('All', 'matrix'),
            'cyrillic' => __('Cyrillic', 'matrix'),
            'cyrillic-ext' => __('Cyrillic Extended', 'matrix'),
            'devanagari' => __('Devanagari', 'matrix'),
            'greek' => __('Greek', 'matrix'),
            'greek-ext' => __('Greek Extended', 'matrix'),
            'khmer' => __('Khmer', 'matrix'),
            'latin' => __('Latin', 'matrix'),
            'latin-ext' => __('Latin Extended', 'matrix'),
            'vietnamese' => __('Vietnamese', 'matrix'),
            'hebrew' => __('Hebrew', 'matrix'),
            'arabic' => __('Arabic', 'matrix'),
            'bengali' => __('Bengali', 'matrix'),
            'gujarati' => __('Gujarati', 'matrix'),
            'tamil' => __('Tamil', 'matrix'),
            'telugu' => __('Telugu', 'matrix'),
            'thai' => __('Thai', 'matrix'),
            'serif' => _x('Serif', 'font style', 'matrix'),
            'sans-serif' => _x('Sans Serif', 'font style', 'matrix'),
            'monospace' => _x('Monospace', 'font style', 'matrix'),
            'font-family' => __('Font Family', 'matrix'),
            'font-size' => __('Font Size', 'matrix'),
            'font-weight' => __('Font Weight', 'matrix'),
            'line-height' => __('Line Height', 'matrix'),
            'letter-spacing' => __('Letter Spacing', 'matrix'),
            'top' => __('Top', 'matrix'),
            'bottom' => __('Bottom', 'matrix'),
            'left' => __('Left', 'matrix'),
            'right' => __('Right', 'matrix'),
        );

        $config = apply_filters('kirki/config', array());

        if (isset($config['i18n'])) {
            $i18n = wp_parse_args($config['i18n'], $i18n);
        }

        return $i18n;

    }

    /**
     * Shortcut method to get the font registry.
     */
    public static function fonts()
    {
        return self::get_instance()->font_registry;
    }

    /**
     * Constructor is private, should only be called by get_instance()
     */
    private function __construct()
    {
    }

    /**
     * Return true if we are debugging Kirki.
     */
    public static function kirki_debug()
    {
        return (bool)(defined('KIRKI_DEBUG') && KIRKI_DEBUG);
    }

    /**
     * Take a path and return it clean
     *
     * @param string $path
     * @return string
     */
    public static function clean_file_path($path)
    {
        $path = str_replace('', '', str_replace(array("\\", "\\\\"), '/', $path));
        if ('/' === $path[strlen($path) - 1]) {
            $path = rtrim($path, '/');
        }
        return $path;
    }

    /**
     * Determine if we're on a parent theme
     *
     * @param $file string
     * @return bool
     */
    public static function is_parent_theme($file)
    {
        $file = self::clean_file_path($file);
        $dir = self::clean_file_path(get_template_directory());
        $file = str_replace('//', '/', $file);
        $dir = str_replace('//', '/', $dir);
        if (false !== strpos($file, $dir)) {
            return true;
        }
        return false;
    }

    /**
     * Determine if we're on a child theme.
     *
     * @param $file string
     * @return bool
     */
    public static function is_child_theme($file)
    {
        $file = self::clean_file_path($file);
        $dir = self::clean_file_path(get_stylesheet_directory());
        $file = str_replace('//', '/', $file);
        $dir = str_replace('//', '/', $dir);
        if (false !== strpos($file, $dir)) {
            return true;
        }
        return false;
    }

    /**
     * Determine if we're running as a plugin
     */
    private static function is_plugin()
    {
        if (false !== strpos(self::clean_file_path(__FILE__), self::clean_file_path(get_stylesheet_directory()))) {
            return false;
        }
        if (false !== strpos(self::clean_file_path(__FILE__), self::clean_file_path(get_template_directory_uri()))) {
            return false;
        }
        if (false !== strpos(self::clean_file_path(__FILE__), self::clean_file_path(WP_CONTENT_DIR . '/themes/'))) {
            return false;
        }
        return true;
    }

    /**
     * Determine if we're on a theme
     *
     * @param $file string
     * @return bool
     */
    public static function is_theme($file)
    {
        if (true == self::is_child_theme($file) || true == self::is_parent_theme($file)) {
            return true;
        }
        return false;
    }
}
